import json
import requests
import time
import random
from vectorops import *

SERVICE_BASE = "http://icfpc2013.cloudapp.net/"
AUTH_KEY = "0522BZjZPRTBqUNYN8ylEKUj41QvBTA1VIzALuwRvpsH1H"

OP1 = {"not":not_, "shl1": shl1_, "shr1":shr1_, "shr4":shr4_, "shr16":shr16_}
OP2 = {"and":and_, "or":or_, "xor":xor_, "plus":plus_}
OP4 = {"if0":if0_}

def get_problems():
    params = {"auth": AUTH_KEY}
    r = requests.get(SERVICE_BASE + "/myproblems", params=params)
    return r.json()

def eval_output(problem_id):
    params = {"auth": AUTH_KEY}
    eval_input = ["0x%016x" % random.randint(0, 0xffffffffffffffff) for i in range(256)]
    payload = {
        "id" : problem_id,
        "arguments" : eval_input} #FIXME: Add arguments
    r = requests.post(SERVICE_BASE + "/eval", data = json.dumps(payload),
                params = params)
    time.sleep(4.5)
    if r.status_code == 200:
        responseDict = r.json()
        if responseDict.get("status") == "ok":
            output = responseDict.get("outputs", [])
            return [(int(x[2:], 16), int(y[2:], 16)) for x, y in zip(eval_input, output)]
        else:
            print "Error with eval!!!"
            print responseDict
    print r.status_code, r.text

def apply_program(expr, arg):
    if type(expr) is long or type(expr) is int:
        return expr
    if type(expr) is str:
        return arg
    else:
        func = expr[0]
        return func(*[apply_program(x, arg) for x in expr[1:]])
    
def is_correct(program, output):
    #print "checking", stringify(program)
    if not output:
        return False
    for i, o in output:
        if apply_program(program, i) != o:
            return False
    return True
    
def verify_syntax(program):
    params = {"auth": AUTH_KEY}
    payload = {
        "program" : program,
        "arguments" : ["0x000000000000001",
                        "0xefffffffffffffff"]}
    r = requests.post(SERVICE_BASE + "/eval", data = json.dumps(payload),
            params = {"auth": AUTH_KEY})
    print r.status_code, r.text

def guess(problem_id, program):
    params = {"auth": AUTH_KEY}
    payload = {
        "id" : problem_id,
        "program" : program
    }
    r = requests.post(SERVICE_BASE + "/guess", data = json.dumps(payload),
            params = {"auth": AUTH_KEY})
    time.sleep(4.5)
    if r.status_code == 200:
        responseDict = r.json()
        if responseDict.get('status') == 'win':
            print "Hurray! 1 point for problem: %s" % problem_id
            return True
        else:
            print "Problem: %s, Response: %s" % (problem_id, responseDict)
            return False
    else:
        print "Problem: %s, Status: %d" % (problem_id, r.status_code)
        return False

def get_training_program(size):
    params = {"auth": AUTH_KEY}
    payload = {
        "size" : size,
        "operators": []
    }
    r = requests.post(SERVICE_BASE + "/train", data = json.dumps(payload),
            params = {"auth": AUTH_KEY})
    assert r.status_code == 200
    time.sleep(4.5)
    return r.json()

def stringifyexpr(expr):
    if type(expr) is long or type(expr) is int:
        return str(expr)
    if type(expr) is str:
        return expr
    else:
        func = expr[0]
        return "(%s %s)" % (func.__name__[:-1], 
                " ".join(stringifyexpr(x) for x in expr[1:]))

def stringify(prog):
    return "(lambda (x) %s)" % stringifyexpr(prog)

def guess_(pid, program):
    print "My guess", stringify(program)

def gen1(*args):
    yield 'x'
    yield 0
    yield 1

def gen2(operators):
    for o, p in OP1.iteritems():
        if o not in operators:
            continue
        for o2 in gen1():
            yield [p, o2]

def gen3(operators):
    for o, p in OP1.iteritems():
        if o not in operators:
            continue
        for expr in gen2(operators):
            yield [p, expr]
    for o, p in OP2.iteritems():
        if o not in operators:
            continue

        # Nice optimization below!
        # Order of the operands is not important for the given op2
        yield [p, 'x', 'x']
        yield [p, 'x', 0]
        yield [p, 'x', 1]
        yield [p, '0', 0]
        yield [p, 1, 0]
        yield [p, 1, 1]

def gen4(operators):
    for o, p in OP1.iteritems():
        if o not in operators:
            continue
        for expr in gen3(operators):
            yield [p, expr]
    for o, p in OP2.iteritems():
        if o not in operators:
            continue
        # We can possibly optimize below. Order is again not important,
        # so duplicate sets, if any, can be removed. Later though!!
        for expr1 in gen2(operators):
            for expr2 in gen1():
                yield [p, expr1, expr2]

    if "if0" in operators:
        for e1 in gen1():
            for e2 in gen1():
                for e3 in gen1():
                    yield [if0_, e1, e2, e3]

def gen5(operators):
    for o, p in OP1.iteritems():
        if o not in operators:
            continue
        for expr in gen4(operators):
            yield [p, expr]
    for o, p in OP2.iteritems():
        if o not in operators:
            continue
        for expr1 in gen2(operators):
            for expr2 in gen2(operators):
                yield [p, expr1, expr2]
        for expr1 in gen3(operators):
            for expr2 in gen1():
                yield [p, expr1, expr2]
    if "if0" in operators:
        for v in ([if0_, e1, e2, e3] for e1 in gen2(operators) for e2 in gen1() for e3 in gen1()):
            yield v
        for v in ([if0_, e1, e2, e3] for e1 in gen1() for e2 in gen2(operators) for e3 in gen1()):
            yield v
        for v in ([if0_, e1, e2, e3] for e1 in gen1() for e2 in gen1() for e3 in gen2(operators)):
            yield v

    # Add fold later

def gen(n, operators):
    if n <= 5:
        return (gen1, gen2, gen3, gen4, gen5)[n-1](operators)
    for o, p in OP1.iteritems():
        if o not in operators:
            continue
        for expr in gen(n-1, operators):
            yield [p, expr]
    for o, p in OP2.iteritems():
        if o not in operators:
            continue
        for i in range(1, n-1):
            for expr1 in gen(i, operators):
                for expr2 in gen(n-i-1, operators):
                    yield [p, expr1, expr2]

    if "if0" in operators:
        for j in range(1, n-3):
            for expr1 in gen(j, operators):
                for i in range(1, n-i-3):
                    for expr2 in gen(i, operators):
                        for expr3 in gen(n-i-j-1, operators):
                            yield [p, expr1, expr2, expr3]

def solve6(program):
    print "solving", program
    pid = program['id']
    ops = set(program['operators'])
    test_cases = eval_output(pid)
    for prog in gen5(ops):
        if is_correct(prog, test_cases):
            print "Guessing", stringify(prog)
            if guess(pid, stringify(prog)):
                break
    else:
        print "We screwed up"
        import sys
        sys.exit(1)
        
def solve_length_6():
    f = open("badprog.json", "r")
    problems = json.load(f)
    f.close()
    length6problems = [i for i in problems if (i.get('size') == 6)]
    for p in length6problems:
        print "solving", p['id'], p.get('challenge', "")
        solve6(p)
        print

def test_len_6():
    while True:
        p = get_training_program(6)
        f = open("badprog.json", "w")
        f.write(json.dumps(p, sort_keys = True, indent=4,
            separators=(',', ': ')))
        f.close()
        solve6(p)
        print

def dump_len_6_progs():
    progs = []
    for i in range(10):
        p = get_training_program(6)
        progs.append(p)
    print json.dumps(progs, sort_keys=True,
            indent=4, separators=(',', ': '))

if __name__ == "__main__":
    #dump_len_6_progs()
    #solve_length_6()
    test_len_6()
    # (lambda (x_5238) (shl1 (and (not 1) x_5238)))
    #solve6({'id': 'kg8KOLyLaOCLrDxBQT6BFlHV', 
    #    'operators': ['shl1', 'and', 'not']})
