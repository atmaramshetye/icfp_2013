import json
import requests
import time
import random
from vectorops import *

SERVICE_BASE = "http://icfpc2013.cloudapp.net/"
AUTH_KEY = "0522BZjZPRTBqUNYN8ylEKUj41QvBTA1VIzALuwRvpsH1H"

OP1 = {"not":not_, "shl1": shl1_, "shr1":shr1_, "shr4":shr4_, "shr16":shr16_}
OP2 = {"and":and_, "or":or_, "xor":xor_, "plus":plus_}

def get_problems():
    params = {"auth": AUTH_KEY}
    r = requests.get(SERVICE_BASE + "/myproblems", params=params)
    return r.json()

def eval_output(problem_id):
    params = {"auth": AUTH_KEY}
    eval_input = ["0x%016x" % random.randint(0, 0xffffffffffffffff) for i in range(256)]
    payload = {
        "id" : problem_id,
        "arguments" : eval_input} #FIXME: Add arguments
    r = requests.post(SERVICE_BASE + "/eval", data = json.dumps(payload),
                params = params)
    time.sleep(5)
    if r.status_code == 200:
        responseDict = r.json()
        if responseDict.get("status") == "ok":
            output = responseDict.get("outputs", [])
            return [(int(x[2:], 16), int(y[2:], 16)) for x, y in zip(eval_input, output)]
        else:
            print "Error with eval!!!"
            print responseDict
    print r.status_code, r.text

def apply_program(expr, arg):
    if type(expr) is long or type(expr) is int:
        return expr
    if type(expr) is str:
        return arg
    else:
        func = expr[0]
        return func(*[apply_program(x, arg) for x in expr[1:]])
    
def is_correct(program, output):
    print "checking", stringify(program), "...", 
    if not output:
        return False
    for i, o in output:
        if apply_program(program, i) != o:
            print "Nope"
            return False
    print "yes"
    return True
    
def verify_syntax(program):
    params = {"auth": AUTH_KEY}
    payload = {
        "program" : program,
        "arguments" : ["0x000000000000001",
                        "0xefffffffffffffff"]}
    r = requests.post(SERVICE_BASE + "/eval", data = json.dumps(payload),
            params = {"auth": AUTH_KEY})
    print r.status_code, r.text

def guess(problem_id, program):
    params = {"auth": AUTH_KEY}
    payload = {
        "id" : problem_id,
        "program" : program
    }
    r = requests.post(SERVICE_BASE + "/guess", data = json.dumps(payload),
            params = {"auth": AUTH_KEY})
    time.sleep(5)
    if r.status_code == 200:
        responseDict = r.json()
        if responseDict.get('status') == 'win':
            print "Hurray! 1 point for problem: %s" % problem_id
            return True
        else:
            print "Problem: %s, Response: %s" % (problem_id, responseDict)
            return False
    else:
        print "Problem: %s, Status: %d" % (problem_id, r.status_code)
        return False

def get_training_program(size):
    params = {"auth": AUTH_KEY}
    payload = {
        "size" : size,
        "operators": []
    }
    r = requests.post(SERVICE_BASE + "/train", data = json.dumps(payload),
            params = {"auth": AUTH_KEY})
    assert r.status_code == 200
    time.sleep(5)
    return r.json()

def stringifyexpr(expr):
    if type(expr) is long or type(expr) is int:
        return str(expr)
    if type(expr) is str:
        return expr
    else:
        func = expr[0]
        return "(%s %s)" % (func.__name__[:-1], 
                " ".join(stringifyexpr(x) for x in expr[1:]))

def stringify(prog):
    return "(lambda (x) %s)" % stringifyexpr(prog)

def guess_(pid, program):
    print "My guess", stringify(program)

def gen3(operators):
    for o, p in OP1.iteritems():
        if o not in operators:
            continue
        yield [p, 'x']

def gen4(operators):
    for o, p in OP1.iteritems():
        if o not in operators:
            continue
        for expr in gen3(operators):
            yield [p, expr]
    for o, p in OP2.iteritems():
        if o not in operators:
            continue
        yield [p, 'x', 'x']
        yield [p, 'x', 0]
        yield [p, 'x', 1]
        yield [p, '0', 0]
        yield [p, 1, 0]
        yield [p, 1, 1]

def gen5(operators):
    for o, p in OP1.iteritems():
        if o not in operators:
            continue
        for expr in gen4(operators):
            yield [p, expr]
    for o, p in OP2.iteritems():
        if o not in operators:
            continue
        for expr in gen3(operators):
            yield [p, expr, 'x']
            yield [p, expr, 0]
            yield [p, expr, 1]

def solve5(program):
    pid = program['id']
    ops = set(program['operators'])
    test_cases = eval_output(pid)
    for prog in gen5(ops):
        if is_correct(prog, test_cases):
            if guess(pid, stringify(prog)):
                break
        
def solve_length_5():
    f = open("problems.out", "r")
    problems = json.load(f)
    f.close()
    length5problems = [i for i in problems if (i.get('size') == 5)]
    for p in length5problems:
        print "solving", p['id']
        solve5(p)
        print

def dump_len_5_progs():
    progs = []
    for i in range(10):
        p = get_training_program(5)
        progs.append(p)
    print json.dumps(progs, sort_keys=True,
            indent=5, separators=(',', ': '))

if __name__ == "__main__":
    #dump_len_5_progs()
    solve_length_5()
