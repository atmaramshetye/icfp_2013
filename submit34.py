import json
import requests
import time
import random
from vectorops import *

SERVICE_BASE = "http://icfpc2013.cloudapp.net/"
AUTH_KEY = "0522BZjZPRTBqUNYN8ylEKUj41QvBTA1VIzALuwRvpsH1H"

OP1 = {"not":not_, "shl1": shl1_, "shr1":shr1_, "shr4":shr4_, "shr16":shr16_}
OP2 = {"and":and_, "or":or_, "xor":xor_, "plus":plus_}

def get_problems():
    params = {"auth": AUTH_KEY}
    r = requests.get(SERVICE_BASE + "/myproblems", params=params)
    return r.json()

def eval_output(problem_id):
    params = {"auth": AUTH_KEY}
    eval_input = ["0x%016x" % random.randint(0, 0xffffffffffffffff) for i in range(256)]
    payload = {
        "id" : problem_id,
        "arguments" : eval_input} #FIXME: Add arguments
    r = requests.post(SERVICE_BASE + "/eval", data = json.dumps(payload),
                params = params)
    time.sleep(5)
    if r.status_code == 200:
        responseDict = r.json()
        if responseDict.get("status") == "ok":
            output = responseDict.get("outputs", [])
            return [(int(x[2:], 16), int(y[2:], 16)) for x, y in zip(eval_input, output)]
        else:
            print "Error with eval!!!"
            print responseDict
    print r.status_code, r.text

def apply_program(expr, arg):
    if type(expr) is long or type(expr) is int:
        return expr
    if type(expr) is str:
        return arg
    else:
        func = expr[0]
        return func(*[apply_program(x, arg) for x in expr[1:]])
    
def is_correct(program, output):
    print "checking", stringify(program), "...", 
    if not output:
        return False
    for i, o in output:
        if apply_program(program, i) != o:
            print "Nope"
            return False
    print "yes"
    return True
    
def verify_syntax(program):
    params = {"auth": AUTH_KEY}
    payload = {
        "program" : program,
        "arguments" : ["0x000000000000001",
                        "0xefffffffffffffff"]}
    r = requests.post(SERVICE_BASE + "/eval", data = json.dumps(payload),
            params = {"auth": AUTH_KEY})
    print r.status_code, r.text

def guess(problem_id, program):
    params = {"auth": AUTH_KEY}
    payload = {
        "id" : problem_id,
        "program" : program
    }
    r = requests.post(SERVICE_BASE + "/guess", data = json.dumps(payload),
            params = {"auth": AUTH_KEY})
    if r.status_code == 200:
        responseDict = r.json()
        if responseDict.get('status') == 'win':
            print "Hurray! 1 point for problem: %s" % problem_id
        else:
            print "Problem: %s, Response: %s" % (problem_id, responseDict)
    else:
        print "Problem: %s, Status: %d" % (problem_id, r.status_code)
    time.sleep(5)

def get_training_program(size):
    params = {"auth": AUTH_KEY}
    payload = {
        "size" : size,
        "operators": []
    }
    r = requests.post(SERVICE_BASE + "/train", data = json.dumps(payload),
            params = {"auth": AUTH_KEY})
    assert r.status_code == 200
    time.sleep(5)
    return r.json()

def stringifyexpr(expr):
    if type(expr) is long or type(expr) is int:
        return str(expr)
    if type(expr) is str:
        return expr
    else:
        func = expr[0]
        return "(%s %s)" % (func.__name__[:-1], 
                " ".join(stringifyexpr(x) for x in expr[1:]))

def stringify(prog):
    return "(lambda (x) %s)" % stringifyexpr(prog)

def guess_(pid, program):
    print "My guess", stringify(program)
        
def solve4(p):
    pid = p.get('id', None)
    print "solving", pid
    assert pid, "No pid found"
    operators = p.get('operators', [])
    assert operators, "No operators found"
    if len(operators) == 1:
        op = operators[0]
        if op in OP1:
            program = [OP1[op], [OP1[op], 'x']] 
            guess(pid, stringify(program))
        elif op in OP2:
            output = eval_output(pid)
            for c in (0, 1):
                #program1 = "(lambda (x) (%s x %d))" % (op, c)
                program1 = [OP2[op], 'x', c]
                if is_correct(program1, output):
                    guess(pid, stringify(program1))
                    return
                #program2 = "(lambda (x) (%s %d x))" % (op, c)
                #if is_correct(program2, output):
                #    guess_(pid, program2)
                #    return
            # (lambda (x) (op x x))
            program = [OP2[op], 'x', 'x']
            if is_correct(program, output):
                guess(pid, stringify(program))
        else:
            print "Cannot understand operator"
    elif len(operators) == 2:
        opA, opB = operators[0], operators[1]
        if opA not in OP1:
            print "Cannot understand operator"
            return
        if opB not in OP1:
            print "Cannot understand operator"
            return
        output = eval_output(pid)
        #program1 = "(lambda (x) (%s (%s x)))" % (opA, opB)
        program1 = [OP1[opA], [OP1[opB], 'x']]
        if is_correct(program1, output):
            guess(pid, stringify(program1))
            return
        #program2 = "(lambda (x) (%s (%s x)))" % (opB, opA)
        program2 = [OP1[opB], [OP1[opA], 'x']]
        if is_correct(program2, output):
            guess(pid, stringify(program2))

def find_and_submit_3_length(problems):
    length3problems = [i for i in problems if (i.get('size') == 3) and (len(i.get('operators', [])) == 1)]
    for p in length3problems:
        op1 = p['operators'][0]
        if not op1:
            continue
        program = "(lambda (x) (%s x))" % op1
        pid = p.get('id')
        if not pid:
            continue
        print pid, program
        guess(pid, program)

def status():
    r = requests.post(SERVICE_BASE + "/status", params = {"auth": AUTH_KEY})
    if r.status_code == 200:
        responseDict = r.json()
        assert responseDict.get("easyChairId") == "522", "Whose status is this?"
        print responseDict.get("contestScore")
    print r.status_code, r.text

def solve_length_4():
    f = open("problems.out", "r")
    problems = json.load(f)
    f.close()
    length4problems = [i for i in problems if (i.get('size') == 4)]
    for p in length4problems:
        solve4(p)
        print

def dump_len_4_progs():
    progs = []
    for i in range(10):
        p = get_training_program(4)
        progs.append(p)
    print json.dumps(progs, sort_keys=True,
            indent=5, separators=(',', ': '))

def dump_problems():
    print json.dumps(get_problems(), sort_keys=True,
            indent=5, separators=(',', ': '))

def test_apply():
    program = [plus_, [shr16_, [not_, 'x']], 1]
    print "0x%016x" % (apply_program(program, 0))
 
if __name__ == "__main__":
    #test_apply()
    solve_length_4()
    #dump_len_4_progs()
    #guess("3XmOXnkaKJMBACBpx6mxuq81", "(lambda (x) (shr1 (shr16 x)))")
