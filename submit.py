import json
import requests
import time
import random
import sys
import itertools
from vectorops import *

def beep():
    try:
        from AppKit import NSBeep
        NSBeep()
    except:
        sys.stdout.write('\a')
        sys.stdout.flush()

SERVICE_BASE = "http://icfpc2013.cloudapp.net/"
AUTH_KEY = "0522BZjZPRTBqUNYN8ylEKUj41QvBTA1VIzALuwRvpsH1H"
REQUEST_TIME = time.time() - 12
WAIT_TIME = 5

OP1 = {"not":not_, "shl1": shl1_, "shr1":shr1_, "shr4":shr4_, "shr16":shr16_}
OP2 = {"and":and_, "or":or_, "xor":xor_, "plus":plus_}

def sleep_request():
    global REQUEST_TIME
    diff = WAIT_TIME - (time.time() - REQUEST_TIME)
    if diff > 0:
        print "sleeping", diff
        time.sleep(diff)
    REQUEST_TIME = time.time()

class NoSolutionException(Exception): pass

def get_problems():
    params = {"auth": AUTH_KEY}
    sleep_request()
    r = requests.get(SERVICE_BASE + "/myproblems", params=params)
    return r.json()

def eval_output(problem_id):
    params = {"auth": AUTH_KEY}
    nums = range(16) + [0xffffffffffffffff, 0x7FFFFFFFFFFFFFFF, 0xFFFFFFFFFFFFFFFE]
    nums += [random.randint(0, 0xffffffffffffffff) for i in range(255 - len(nums))]
    eval_input = ["0x%016x" % num for num in nums]
    eval_input
    payload = {
        "id" : problem_id,
        "arguments" : eval_input} #FIXME: Add arguments
    sleep_request()
    r = requests.post(SERVICE_BASE + "/eval", data = json.dumps(payload),
                params = params)
    if r.status_code == 200:
        responseDict = r.json()
        if responseDict.get("status") == "ok":
            output = responseDict.get("outputs", [])
            return [(int(x[2:], 16), int(y[2:], 16)) for x, y in zip(eval_input, output)]
        else:
            print "Error with eval!!!"
            print responseDict
    print r.status_code, r.text

def apply_program(expr, arg, fargy, fargz):
    #print "call", stringifyexpr(expr), repr(expr), repr(arg), fargy, fargz
    if type(expr) is int or type(expr) is long:
        return expr
    if type(expr) is str:
        if expr == 'x':
            return arg
        elif expr == 'y':
            return fargy
        else:
            return fargz
    else:
        #print stringifyexpr(expr)
        func = expr[0]
        if func == "fold":
            val = apply_program(expr[1], arg, None, None)
            v0 = apply_program(expr[2], arg, None, None)
            for i in range(0, 64, 8):
                v0 = apply_program(expr[3], None, (val & (0xff << i)) >> i, v0)
            return v0
        else:
            #return func(*[apply_program(x, arg, fargy, fargz) for x in expr[1:]])
            args = [apply_program(x, arg, fargy, fargz) for x in expr[1:]]
            #print "new args", args, repr(expr[1:])
            return func(*args)
    
def is_correct(program, output):
    # print "checking", stringify(program)
    if not output:
        return False
    for i, o in output:
        if apply_program(program, i, None, None) != o:
            return False
    return True
    
def verify_syntax(program):
    params = {"auth": AUTH_KEY}
    payload = {
        "program" : program,
        "arguments" : ["0x000000000000001",
                        "0xefffffffffffffff"]}
    sleep_request()
    r = requests.post(SERVICE_BASE + "/eval", data = json.dumps(payload),
            params = {"auth": AUTH_KEY})
    print r.status_code, r.text

def guess(problem_id, program):
    params = {"auth": AUTH_KEY}
    payload = {
        "id" : problem_id,
        "program" : program
    }
    sleep_request()
    r = requests.post(SERVICE_BASE + "/guess", data = json.dumps(payload),
            params = {"auth": AUTH_KEY})
    responseDict = r.json()
    if r.status_code == 200:
        if responseDict.get('status') == 'win':
            print "Hurray! 1 point for problem: %s" % problem_id
            return True, []
    print "Problem: %s, Program: %s, Response: %s" % (problem_id, program, responseDict)
    if responseDict.get('status') == 'mismatch':
        v = responseDict['values']
        return False, [(int(v[0][2:], 16), int(v[1][2:], 16))]
    return False, []

def get_training_program(size):
    params = {"auth": AUTH_KEY}
    payload = {
        "size" : size,
        "operators": ["tfold"]
    }
    sleep_request()
    r = requests.post(SERVICE_BASE + "/train", data = json.dumps(payload),
            params = {"auth": AUTH_KEY})
    print r.status_code, r.text
    assert r.status_code == 200
    return r.json()

def stringifyexpr(expr):
    if type(expr) is long or type(expr) is int:
        return str(expr)
    if type(expr) is str:
        return expr
    else:
        func = expr[0]
        if func == 'fold':
            return "(fold %s %s (lambda (y z) %s))" % (stringifyexpr(expr[1]),
                    stringifyexpr(expr[2]), stringifyexpr(expr[3]))
        else:
            return "(%s %s)" % (func.__name__[:-1], 
                    " ".join(stringifyexpr(x) for x in expr[1:]))

def stringify(prog):
    return "(lambda (x) %s)" % stringifyexpr(prog)

def guess_(pid, program):
    print "My guess", stringify(program)

def gen1(operators, infold, folddone = None):
    if infold:
        yield 'y'
        yield 'z'
    else:
        yield 'x'
    yield 0
    yield 1

def gen2(operators, infold, folddone = None):
    for o, p in OP1.iteritems():
        if o not in operators:
            continue
        for o2 in gen1(operators, infold):
            yield [p, o2]

def gen3(operators, infold, folddone = None):
    for o, p in OP1.iteritems():
        if o not in operators:
            continue
        for expr in gen2(operators, infold):
            yield [p, expr]
    for o, p in OP2.iteritems():
        if o not in operators:
            continue

        # Nice optimization below!
        # Order of the operands is not important for the given op2
        if not infold:
            yield [p, 'x', 'x']
            yield [p, 'x', 0]
            yield [p, 'x', 1]
        else:
            yield [p, 'y', 'y']
            yield [p, 'y', 0]
            yield [p, 'y', 1]
            yield [p, 'z', 'z']
            yield [p, 'z', 0]
            yield [p, 'z', 1]
            yield [p, 'y', 'z']

        yield [p, 0, 0]
        yield [p, 1, 0]
        yield [p, 1, 1]

def gen4(operators, infold, folddone = None):
    for o, p in OP1.iteritems():
        if o not in operators:
            continue
        for expr in gen3(operators, infold):
            yield [p, expr]
    for o, p in OP2.iteritems():
        if o not in operators:
            continue
        # We can possibly optimize below. Order is again not important,
        # so duplicate sets, if any, can be removed. Later though!!
        for expr1 in gen2(operators, infold):
            for expr2 in gen1(operators, infold):
                yield [p, expr1, expr2]

    #if "if0" in operators:
    #    for e1 in gen1():
    #        for e2 in gen1():
    #            for e3 in gen1():
    #                yield [if0_, e1, e2, e3]
    if "if0" in operators:
        if not infold:
            yield [if0_, 'x', 0, 1]
            yield [if0_, 'x', 1, 0]
            yield [if0_, 'x', 0, 'x']
        else:
            for e1 in gen1(operators, infold):
                for e2 in gen1(operators, infold):
                    for e3 in gen1(operators, infold):
                        yield [if0_, e1, e2, e3]

def gen5(operators, infold, folddone):
    for o, p in OP1.iteritems():
        if o not in operators:
            continue
        for expr in gen4(operators, infold):
            yield [p, expr]
    for o, p in OP2.iteritems():
        if o not in operators:
            continue
        for expr1 in gen2(operators, infold):
            for expr2 in gen2(operators, infold):
                yield [p, expr1, expr2]
        for expr1 in gen3(operators, infold):
            for expr2 in gen1(operators, infold):
                yield [p, expr1, expr2]
    if "if0" in operators:
        for v in ([if0_, e1, e2, e3] for e1 in gen2(operators, infold) for e2 in gen1(operators, infold) for e3 in gen1(operators, infold)):
            yield v
        for v in ([if0_, 'x', e2, e3] for e2 in gen2(operators, infold) for e3 in gen1(operators, infold)):
            yield v
        for v in ([if0_, 'x', e2, e3] for e2 in gen1(operators, infold) for e3 in gen2(operators, infold)):
            yield v

def gen(n, operators, infold, folddone):
    if n <= 5:
        for v in (gen1, gen2, gen3, gen4, gen5)[n-1](operators, infold, folddone):
            yield v
    else:
        for o, p in OP1.iteritems():
            if o not in operators:
                continue
            for expr in gen(n-1, operators, infold, folddone):
                yield [p, expr]
        for o, p in OP2.iteritems():
            if o not in operators:
                continue
            for i in range(1, n-1):
                for expr1 in gen(i, operators, infold, folddone):
                    for expr2 in gen(n-i-1, operators, infold, folddone):
                        yield [p, expr1, expr2]

        if "if0" in operators:
            for j in range(1, n-1):
                for expr1 in gen(j, operators, infold, folddone):
                    if expr1 == 0 or expr1 == 1:
                        continue
                    for i in range(1, n-j-1):
                        for expr2 in gen(i, operators, infold, folddone):
                            for expr3 in gen(n-i-j-1, operators, infold, folddone):
                                yield [if0_, expr1, expr2, expr3]

        if n >= 7 and not folddone:
            if "fold" in operators:
                for j in range(1, n-5):
                    for expr1 in gen(j, operators, False, True):
                        for i in range(1, n-j-4):
                            for expr2 in gen(i, operators, False, True):
                                for expr3 in gen(n-i-j-2, operators, True, True):
                                    yield ["fold", expr1, expr2, expr3]
            if "tfold" in operators:
                for j in range(1, n-5):
                    for expr1 in gen(j, operators, False, True):
                        for expr3 in gen(n-j-2, operators, True, True):
                            yield ["fold", expr1, 0, expr3]

def solve(program):
    print "solving", program
    pid = program['id']
    ops = set(program['operators'])
    test_cases = eval_output(pid)
    #for prog in gen(program['size'] - 1, ops):
    counter = 0
    start = time.time()
    MAX_TIME = 305
    for prog in itertools.chain(gen(8, ops, False, False), gen(9, ops, False, False), gen(10, ops, False, False)):
        counter += 1
        if counter % 20000 == 0:
            if (time.time() - start) > MAX_TIME:
                print "Timed out"
                raise NoSolutionException()
        if is_correct(prog, test_cases):
            print "Guessing", stringify(prog)
            status, cases = guess(pid, stringify(prog))
            if status == True:
                break
            test_cases += cases
    else:
        print "We screwed up"
        with file("failedcases.lst", "w") as f:
            f.write(json.dumps(test_cases, sort_keys = True, indent=4,
                    separators=(',', ': ')))
        raise NoSolutionException()
        
def solve_all(size, filename):
    f = open(filename, "r")
    problems = json.load(f)
    f.close()
    #problems = [i for i in problems if (i.get('size') == size)]
    # sort problems by size
    problems.sort(key = lambda x: x.get('size'))
    good = 0
    count = 0
    for p in problems:
        print "Problem", p['id'], p.get("challenge", ""), "...", 
        if "bonus" in p["operators"]:
            print "skipping because of BONUS"
            continue
        #if "fold" in p['operators'] or "tfold" in p["operators"] or "bonus" in p["operators"]:
        #    print "skipping because of FOLD|BONUS"
        #    continue
        if p.get('solved', False) == True:
            print "already solved"
            continue
        if p.get('timeLeft', 1) == 0:
            print "timed out"
            continue
        print "solving"
        try:
            solve(p)
            good += 1
        except NoSolutionException, e:
            print ">>>>>>>>>>>>> no solution found"
            time.sleep(5)
            beep()
        count += 1
        p = float(good)/count
        print "Success rate = %d/%d ( %0.2f%% )" % (good, count, p*100)
        if count > 15 and p < 0.7:
            print "**** breaking because p too low"
            break
        print

def test_progs(size):
    while True:
        p = get_training_program(size)
        try:
            solve(p)
        except NoSolutionException, e:
            beep()
            with file("badprogs.json", "r+") as f:
                progs = json.loads(f.read())
                progs.append(p)
                f.seek(0)
                f.write(json.dumps(progs, sort_keys = True, indent=4,
                separators=(',', ': ')))
            raw_input("press enter to continue")
        print

def dump_progs(size):
    progs = []
    for i in range(10):
        p = get_training_program(size)
        progs.append(p)
    print json.dumps(progs, sort_keys=True,
            indent=4, separators=(',', ': '))

def benchmark():
    ops = ["if0","not","plus","shl1","xor"]
    i = 0
    for prog in itertools.chain(gen(9, ops)):
        apply_program(prog, 0, None, None)
        i += 1
    print "count", i

def print_usage():
    print "Usage: python submit.py <test|debug|submit> size"

if __name__ == "__main__":
    #benchmark()
    #sys.exit(1)
    if len(sys.argv) != 3:
        print_usage()
        sys.exit(1)

    size = int(sys.argv[2])
    if sys.argv[1] == "test":
        test_progs(size)
    elif sys.argv[1] == "debug":
        solve_all(size, "badprogs.json")
    elif sys.argv[1] == "submit":
        solve_all(size, "problems.out")
    else:
        print_usage()
