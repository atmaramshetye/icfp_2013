# All operations that might potentially overflow 64 bits are clipped

def not_(arg):
    return arg ^ 0xffffffffffffffff

def shl1_(arg):
    return (arg << 1) & 0xffffffffffffffff

def shr1_(arg):
    return arg >> 1

def shr4_(arg):
    return arg >> 4

def shr16_(arg):
    return arg >> 16

def and_(arg1, arg2):
    return arg1 & arg2 

def or_(arg1, arg2):
    return arg1 | arg2 

def xor_(arg1, arg2):
    return arg1 ^ arg2

def plus_(arg1, arg2):
    return (arg1 + arg2) & 0xffffffffffffffff

def if0_(arg1, arg2, arg3):
    return (arg2 if arg1 == 0 else arg3)

# TODO: implement fold
def fold_(arg, acc, f):
    acc = f(arg & 0x00000000000000ff, acc)
    acc = f((arg & 0x000000000000ff00) >> 8, acc)
    acc = f((arg & 0x0000000000ff0000) >> 16, acc)
    acc = f((arg & 0x00000000ff000000) >> 24, acc)
    acc = f((arg & 0x000000ff00000000) >> 32, acc)
    acc = f((arg & 0x0000ff0000000000) >> 40, acc)
    acc = f((arg & 0x00ff000000000000) >> 48, acc)
    acc = f((arg & 0xff00000000000000) >> 56, acc)
    return acc

if __name__ == '__main__':
    assert fold_(0x1122334455667788, 0, or_) == (0x11|0x22|0x33|0x44|0x55|0x66|0x77|0x88)
